from django.db import models
from django.utils import timezone
from user.models import Profile
from django.utils.text import slugify
from django.shortcuts import reverse

from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField

from extensions.utils import jalali_convertor
from django.contrib.auth.models import User





class Post(models.Model):
    title       = models.CharField(max_length = 50, verbose_name = 'تیتر')
    body        = RichTextUploadingField(blank=True, null=True, verbose_name = 'توضیحات')
    image       = models.ImageField(blank = True, null=True, verbose_name='تصویر')
    create_date = models.DateTimeField(auto_now_add = True, editable=False, verbose_name = 'تاریخ ساخت پست')
    update_date = models.DateTimeField(editable = False, verbose_name = 'تاریخ آپدیت پست')
    user        = models.ForeignKey(User, related_name='user_post',verbose_name = 'پروفایل', on_delete=models.SET_NULL, null = True)
    slug        = models.SlugField(verbose_name = 'اسلاگ', allow_unicode=True)
    publish     = models.BooleanField(default = False)
    likes       = models.ManyToManyField(User, related_name = 'likepost', blank = True)
    tag         = models.TextField(verbose_name = 'برچسب')

    def save(self, *args, **kwargs):
        if not self.create_date:
            self.create_date = timezone.now()

        self.update_date = timezone.now()
        # self.slug = slugify(self.title)
        return super(Post, self).save(*args, **kwargs)

    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return f"{self.title} : {self.user}"

    def jalali_date(self):
        return jalali_convertor(self.create_date)

    class Meta:
        ordering = ['-create_date']
        verbose_name = 'پست'
        verbose_name_plural = 'پست‌ها'



