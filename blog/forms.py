from django import forms
from blog.models import Post
from django.forms import ModelForm
from comment.models import Comment



class UserCommentForm(forms.Form):
    text = forms.CharField(label='دیدگاه', widget=forms.Textarea(
        attrs={
            'placeholder': '....دیدگاه خود را وارد کنید',
            'autocomplete':'off'
        }
    ))



class CreatePostForm(ModelForm):
    class Meta:
        model = Post
        fields = (
            'title',
            'image',
            'body',
            'tag',
        )






class AddReplyForm(ModelForm):
	class Meta:
		model = Comment
		fields = ('text',)
