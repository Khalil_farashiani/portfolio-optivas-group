from user.models import Profile
from django.views.generic.list import ListView
from django.db.models import Count
from django.views.generic.detail import DetailView
from .models import Post
from comment.models import Comment
from .forms import UserCommentForm, CreatePostForm
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django import http
from django.views.generic.edit import UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import AddReplyForm
from django.core.cache import cache
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext




CACHE_TTL = getattr(settings, 'CACHE_TTL', 10)

class Home(ListView):
    model = Post
    paginate_by = 2
    template_name = 'blog/home.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newposts' in cache:
            newposts = cache.get('newposts')
            starposts = cache.get('starposts')
            context['newposts'] = newposts
            context['starposts'] = starposts
        else:
            newposts = Post.objects.filter(publish=True).order_by('create_date__hour')
            starposts = Post.objects.annotate(like_count=Count('likes')).order_by('-like_count')[:3]
            context['newposts'] = newposts
            context['starposts'] = starposts
            cache.set('newposts', newposts, timeout=CACHE_TTL)
            cache.set('starposts', starposts, timeout=CACHE_TTL)
        return context




class SearchView(ListView):
    model = Post
    template_name = 'blog/searchresult.html'
    context_object_name = 'all_search_results'

    def get_queryset(self):
        result = super(SearchView, self).get_queryset()
        query = self.request.GET.get('search')
        if query:
            postresult = Post.objects.filter(title__contains=query)
            result = postresult
        else:
            result = None
        return result



def DetailPost(request, id, slug):
    if 'post' in cache:
        post = cache.get('post')
        comments = cache.get('comments')
        total_likes = cache.get('total_likes')
    else:
        post = Post.objects.get(id=id, slug=slug)  
        # comments = Comment.objects.all().order_by('-create_date')
        comments = Comment.objects.filter(post=post).order_by('-create_date')
        total_likes = post.total_likes()

        cache.set('post', post, timeout=CACHE_TTL)
        cache.set('comments', comments, timeout=CACHE_TTL)
        cache.set('total_likes', total_likes, timeout=CACHE_TTL)


    Liked = False
    bookmarked = False
    if post.likes.filter(id = request.user.id).exists():
        Liked = True
    
    if request.user.is_authenticated:
        profile = get_object_or_404(Profile, id = request.user.id)
        if profile.bookmark.filter(id = post.id).exists():
            bookmarked = True
    if request.method == "POST" and request.user.is_authenticated:
        form = UserCommentForm(request.POST)
        if form.is_valid():
             
            cd   = form.cleaned_data
            c1   = Comment.objects.create(post=post, user = request.user, text=cd['text'])
            c1.save()
            messages.success(request, 'دیدگاه شما با موفقیت ثبت شد')
            # return http.HttpResponseRedirect('')
            return redirect('blog:detail', id=post.id,slug=post.slug)
    
    elif request.method == "POST" and not request.user.is_authenticated:
        messages.error(request,"شما ابتدا باید لاگین کنید")
        form = UserCommentForm()
    else:
        form = UserCommentForm()
        


    context = {
        'post' : post,
        'comments': comments,
        'form': form,
        'total_likes' : total_likes,
        'Liked' : Liked,
        'bookmarked' : bookmarked,
    }
    return render(request, 'blog/PostDetail.html', context)





def add_reply(request, post_id, comment_id):
    post    = get_object_or_404(Post, id=post_id)
    comment = get_object_or_404(Comment, id=comment_id)
    if request.method == "POST":
        form = AddReplyForm(request.POST)
        if form.is_valid():
            rep_com = form.save(commit=False)
            rep_com.user = request.user
            rep_com.post = post
            rep_com.username_reply = comment.user.username
            rep_com.is_reply = True
            rep_com.save()
            messages.success(request, 'دیدگاه شما با موفقیت ارسال شد', 'success')
            return redirect('blog:detail', id=post.id,slug=post.slug)
    else:
        form = AddReplyForm()

    context = {
        'form': form
    }
    return render(request, 'blog/add_reply.html', context)
            








@login_required
def like(request, id, slug):
    pk = request.POST.get('post_id')
    post = get_object_or_404(Post, id = pk)
    if post.likes.filter(id = request.user.id).exists():
        post.likes.remove(request.user)
    else:
        post.likes.add(request.user)
    return redirect('blog:detail', id=id, slug=slug)



def contact_us(request):
    return render(request, 'blog/contact.html')



class UpdateComment(UpdateView):
    model = Comment
    fields = ['text']
    template_name = 'blog/update_comment.html'
    
    def get_object(self):
        return get_object_or_404(Comment.objects.filter(id=self.kwargs.get("id")))


    def get_success_url(self):
        c = Comment.objects.get(id=self.kwargs.get("id"))
        id_post = c.post.id
        slug_post = c.post.slug
        return reverse_lazy('blog:detail', kwargs={'id': id_post, 'slug': slug_post})



    


class DeleteComment(DeleteView):
    model = Comment
    template_name = 'blog/delete_comment.html'


    def get_object(self):
        return get_object_or_404(Comment.objects.filter(id=self.kwargs.get("id")))

    def get_success_url(self):
        c = Comment.objects.get(id=self.kwargs.get("id"))
        id_post = c.post.id
        slug_post = c.post.slug
        return reverse_lazy('blog:detail', kwargs={'id': id_post, 'slug': slug_post})







@login_required
def CreatePost(request):
    if request.method == 'POST':
        post_form = CreatePostForm(request.POST)
        if post_form.is_valid():
            post = post_form.save(commit = False)
            post.user = request.user
            post_form.save()
            messages.success(request, 'پست با موفقیت ساخته شد')
            return redirect('blog:Home')
    else:
        post_form = CreatePostForm()
    return render(request, 'blog/CreatePost.html', {'form_post': post_form})



def handl404(request, exception, template_name="404.html"):
    response = render_to_response(template_name)
    response.status_code = 404
    return response