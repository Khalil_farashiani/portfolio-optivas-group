from django.urls import path, re_path

from .views import Home, SearchView, DetailPost, contact_us, UpdateComment, DeleteComment

from .views import Home, SearchView, DetailPost, contact_us, like, CreatePost, add_reply
from django.conf.urls import handler404

handler404 = 'blog.views.handl404'


app_name = 'blog'


urlpatterns = [
    path('home/', Home.as_view(), name='Home'),
    path('<int:id>/update/', UpdateComment.as_view(), name='update'),
    path('results/', SearchView.as_view(), name='search'),
    re_path(r'detail/(?P<id>[0-9]+)/(?P<slug>[-\w]+)/', DetailPost, name='detail'),
    path('contact/', contact_us, name='contact'),
    path('<int:id>/delete/', DeleteComment.as_view(), name='delete'),
    re_path(r'like/(?P<id>[0-9]+)/(?P<slug>[-\w]+)/', like, name='like_post'),
    path('create_post/', CreatePost, name = 'create_post'),
    path('add_reply/<int:post_id>/<int:comment_id>/', add_reply, name='reply'),
    ]

