from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', include('blog.urls')),
    path('', include('user.urls')),
    path('captcha/', include('captcha.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    prefix_default_language = False,
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# urlpatterns += static(settings.STATICFILES_DIRS, document_root=settings.STATICFILES_DIRS)