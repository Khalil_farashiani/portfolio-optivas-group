from . import jalali


def jalali_convertor(time):
    list_month = [
        'فروردین',
        'اردیبهشت',
        'خرداد',
        'تیر',
        'مرداد',
        'شهریور',
        'مهر',
        'آبان',
        'آذر',
        'دی',
        'بهمن',
        'اسفند',
    ]
    jdate =  list(jalali.Gregorian(f'{time.year},{time.month},{time.day}').persian_tuple())

    month = list_month[int(jdate[1])-1]

    output = """ساعت{}:{}   || {}  {}  {}""".format(
        time.hour,
        time.minute,
        jdate[2],
        month,
        jdate[0],
        
        
    )
    return output
    


