from django.db import models
from user.models import Profile
from blog.models import Post
from django.contrib.auth.models import User


class Comment(models.Model):
    user           = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='user', null=True, blank=True, verbose_name = 'کاربر')
    post           = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='post',verbose_name = 'پست')
    text           = models.TextField(max_length=400, verbose_name = 'متن')
    create_date    = models.DateTimeField(auto_now_add=True, editable = False, verbose_name='تاریخ ساخت کامنت')
    update_date    = models.DateTimeField(auto_now=True, editable=False, verbose_name = 'تاریخ آپدیت کامنت')
    is_reply       = models.BooleanField(default=False, verbose_name = 'ریپلای شده')
    username_reply = models.CharField(null = True, blank = True, max_length=50, verbose_name ='نام کاربری ریپلای شده')
    publish        = models.BooleanField(default = False, verbose_name = 'انتشار')
    like           = models.ManyToManyField(Profile, blank = True, related_name ='likecomment', verbose_name='لایک')

    class Meta:
        verbose_name = 'کامنت'
        verbose_name_plural = 'کامنت‌ها'

    def __str__(self):
        return self.user.username
