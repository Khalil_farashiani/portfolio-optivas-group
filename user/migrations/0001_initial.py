# Generated by Django 2.2.5 on 2021-07-21 02:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import user.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='social',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('github', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.GithubValidator], verbose_name='گیت\u200cهاب')),
                ('gitlab', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.GitlabValidator], verbose_name='گیت\u200c\u200cلب')),
                ('instagram', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.InstagramValidator], verbose_name='اینستاگرام')),
                ('tweeter', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.TwitterValidator], verbose_name='تويیتر')),
                ('linkedin', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.LinkedinValidator], verbose_name='لینکدین')),
                ('telegram', models.CharField(blank=True, max_length=50, null=True, validators=[user.validators.TelegramValidator], verbose_name='تلگرام')),
            ],
            options={
                'verbose_name': 'شبکه اجتماعی',
                'verbose_name_plural': 'شبکه\u200cهای اجتماعی',
            },
        ),
        migrations.CreateModel(
            name='UserImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avatar', models.ImageField(blank=True, default='avatar_pic/profile.png', null=True, upload_to='avatar_pic', verbose_name='آواتار')),
                ('header_image', models.ImageField(blank=True, default='header_pic/header.png', null=True, upload_to='header_pic', verbose_name='تصویر هدر')),
            ],
            options={
                'verbose_name': 'تصویر کاربر',
                'verbose_name_plural': 'تصاویر کاربر',
            },
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('from_user', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='follower', to=settings.AUTH_USER_MODEL, verbose_name='فالوور')),
                ('to_user', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='following', to=settings.AUTH_USER_MODEL, verbose_name='فالووینک')),
            ],
            options={
                'verbose_name': 'ارتباط',
                'verbose_name_plural': 'ارتباطات',
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_number', models.CharField(max_length=50, verbose_name='شماره تماس')),
                ('biography', models.TextField(blank=True, max_length=250, null=True, verbose_name='بیوگرافی')),
                ('bookmark', models.ManyToManyField(blank=True, related_name='bookmark', to='blog.Post', verbose_name='بوک مارک')),
                ('image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='user.UserImage', verbose_name='تصویر')),
                ('social', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='user.social', verbose_name='شبکه اجتماعی')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_profile', to=settings.AUTH_USER_MODEL, verbose_name='کاربر')),
            ],
            options={
                'verbose_name': 'پروفایل',
                'verbose_name_plural': 'پروفایل\u200cها',
            },
        ),
    ]
