from django.contrib.syndication.views import Feed
from blog.models import Post


class NewPostFollowing(Feed):
    title = "New post"
    link = 'http//127.0.0.1:8000'
    description = "New awesome post from your following"

    def get_object(self, request, user_id):
        return Post.objects.filter(user_id = user_id)[:1]

    def items(self):
        return Post.objects.all()

    def title_post(self, item):
        return item.title

    def item_description(self, item):
        return item.body

    def item_link(self, item):
        return f'http//127.0.0.1:8000/{item.id}/{item.slug}' 

    
