from .views import User, user_dashboard, signup, CompleteSignUp, follow, unfollow
from django.urls import path, re_path
from django.conf.urls import url
from .feeds import NewPostFollowing
from django.urls import reverse_lazy
from .views import (User,
                    user_dashboard,
                    signup,
                    CompleteSignUp,
                    follower,
                    following,
                    bookmark,
                    user_bookmark
                )
from django.contrib.auth.views import (
                                        LoginView,
                                        LogoutView,
                                    )                                    

app_name = 'user'


urlpatterns = [
    path('userdashboard/<int:user_id>/follow/', follow, name='follow'),
	path('userdashboard/<int:user_id>/unfollow/', unfollow, name='unfollow'),
    path('users/', User.as_view(), name='User'),
    path('signup/', signup, name='signup'),
    path('userdashboard/<int:user_id>/', user_dashboard, name='dashboard'),
    path('profile/',CompleteSignUp, name='create profile'),
    path('login/', LoginView.as_view(template_name = 'user/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('followers/<int:id>', follower, name='follower'),
    path('followings/<int:id>', following, name='following'),
    re_path('bookmark/(?P<id>[0-9]+)/(?P<slug>[-\w]+)/', bookmark, name='bookmark'),
    path('your_bookmarks/', user_bookmark, name='user_bookmark'),
    path('beats/<int:user_id>/rss/', NewPostFollowing()),    
]
