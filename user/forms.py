from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Profile, social, UserImage
from captcha.fields import CaptchaField



class SignUpForm(UserCreationForm):
    email   = forms.EmailField(max_length=254, help_text='requierd')
    captcha = CaptchaField()

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'password1',
            'password2', 
            )

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            'phone_number',
            'biography',
            )


class ImageProfileForm(ModelForm):

    class Meta:
        model = UserImage
        fields = (
            'avatar',
            'header_image',
            )


class SocialProfileForm(ModelForm):
    class Meta:
        model = social
        fields = '__all__'

