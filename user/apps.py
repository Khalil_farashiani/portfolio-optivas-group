from django.apps import AppConfig

class UserConfig(AppConfig):
    name = 'user'
    verbose_name = 'کاربر'

    def ready(self):
        from . import signals



    