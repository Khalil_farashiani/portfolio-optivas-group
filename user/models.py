from django.db import models
from django.contrib.auth.models import User
from .validators import (
                        TelegramValidator,
                        TwitterValidator,
                        InstagramValidator,
                        TwitterValidator,
                        LinkedinValidator,
                        GithubValidator,
                        GitlabValidator,
                        )

class UserImage(models.Model):
    avatar       = models.ImageField(blank=True, null=True,\
                                        upload_to='avatar_pic',\
                                        verbose_name = 'آواتار',
                                        default = 'avatar_pic/profile.png'
                                        )
    header_image = models.ImageField(blank=True, null=True,\
                                        upload_to='header_pic',\
                                        verbose_name = 'تصویر هدر',
                                        default = 'header_pic/header.png'
                                        )

    class Meta:
        verbose_name = 'تصویر کاربر'
        verbose_name_plural = 'تصاویر کاربر'




class social(models.Model):
    github    = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'گیت‌هاب', validators=[GithubValidator,]
                                )
    gitlab    = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'گیت‌‌لب', validators=[GitlabValidator,]
                                )
    instagram = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'اینستاگرام', validators=[InstagramValidator,]
                                )
    tweeter   = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'تويیتر', validators=[TwitterValidator,]
                                )
    linkedin  = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'لینکدین', validators=[LinkedinValidator,]
                                )
    telegram  = models.CharField(max_length=50, null=True, blank=True,
                                verbose_name = 'تلگرام', validators=[TelegramValidator,]
                                )

    class Meta:
        verbose_name = 'شبکه اجتماعی'
        verbose_name_plural = 'شبکه‌های اجتماعی'





class Profile(models.Model):
    user         = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name = 'کاربر', related_name= 'profile')
    image        = models.OneToOneField(UserImage, on_delete=models.SET_NULL, null=True, blank=True, verbose_name = 'تصویر')
    phone_number = models.CharField(max_length=50, verbose_name='شماره تماس')
    biography    = models.TextField(max_length=250, verbose_name='بیوگرافی', null=True, blank=True)
    social       = models.OneToOneField(social, on_delete=models.SET_NULL, null=True, blank=True, verbose_name = 'شبکه اجتماعی')
    bookmark     = models.ManyToManyField('blog.Post', related_name='bookmark', blank=True, verbose_name = 'بوک مارک')

    class Meta:
        verbose_name = 'پروفایل'
        verbose_name_plural = 'پروفایل‌ها'

    def __str__(self):
        return self.user.username


class Relation(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower', verbose_name='فالوور', blank=True)
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following', verbose_name='فالووینک', blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)
        verbose_name = 'ارتباط'
        verbose_name_plural = 'ارتباطات'


    def __str__(self):
        return f'{self.from_user} following {self.to_user}'


