from django.contrib.auth.models import User
from django.views.generic.list import ListView
from blog.models import Post
from django.shortcuts import get_object_or_404, render
from .models import Profile
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import SignUpForm, ProfileForm, SocialProfileForm, ImageProfileForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User as UserModel
from .models import Relation
from django.http import JsonResponse



class User(ListView):
    model = Post
    paginate_by = 6
    template_name = 'user/user.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["postuser"] = Post.objects.filter(user__user=self.request.user)
        print(context)
        return context




def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            messages.success(request, 'اکانت با موفقیت ساخته شد')
            return redirect('blog:Home')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})




@login_required
def CompleteSignUp(request):
    if request.method == 'POST':
        profile = get_object_or_404(Profile, id=request.user.id)
        profile_form = ProfileForm(request.POST,instance=profile)
        image_profile_form = ImageProfileForm(request.POST, request.FILES,instance=profile.image)
        social_profile_form = SocialProfileForm(request.POST, instance=profile.social)
        if profile_form.is_valid() and image_profile_form.is_valid() and social_profile_form.is_valid():
            profile = profile_form.save(commit=False)
            social  = social_profile_form.save()
            images  = image_profile_form.save()
            profile.user = request.user
            profile.social = social
            profile.image = images
            profile.save()
            messages.success(request, 'پروفایل با موفقیت کامل شد')
            return redirect('blog:Home')
    else:
        profile = get_object_or_404(Profile, id = request.user.id)
        profile_form = ProfileForm(
                                    initial={
                                        'phone_number': profile.phone_number,
                                        'biography':profile.biography
                                        }
                                )
        if profile.image:
            image_profile_form = ImageProfileForm(
                                                initial={
                                                    'avatar':profile.image.avatar,
                                                    'header_image':profile.image.header_image
                                                }
                                        )
        else:
            image_profile_form = ImageProfileForm()
        if profile.social:
            social_profile_form = SocialProfileForm(
                                                initial={
                                                    'github':profile.social.github,
                                                    'gitlab':profile.social.gitlab,
                                                    'instagram':profile.social.instagram,
                                                    'tweeter':profile.social.tweeter,
                                                    'linkedin':profile.social.linkedin,
                                                    'telegram':profile.social.telegram,
                                                }
                                            )
        else:
            social_profile_form = SocialProfileForm()
        
    return render(request, 'user/createprofile.html', {'form_p': profile_form, 'form_i': image_profile_form, 'form_s': social_profile_form})



def user_dashboard(request, user_id):
    user = get_object_or_404(UserModel, id=user_id)
    posts = Post.objects.filter(user_id = user.id)
    self_panel = False
    is_following = False
    relation = Relation.objects.filter(from_user = request.user.id, to_user=user)
    if relation.exists():
        is_following = True
    if request.user.id == user_id:
        self_panel = True
    x = {'user': user, 'posts': posts, 'self_dash': self_panel, 'is_following': is_following}
    return render(request, 'user/dashboard.html',
                  {'user': user, 'posts': posts, 'self_dash': self_panel, 'is_following': is_following})






# ============================================== follow ========================================================
@login_required
def follow(request, user_id):
	if request.method == 'POST':
		user_id = request.POST['user_id']
		following = get_object_or_404(UserModel, pk=user_id)
		check_relation = Relation.objects.filter(from_user=request.user, to_user=following)
		if check_relation.exists():
			return JsonResponse({'status':'exists'})
		else:
			Relation(from_user=request.user, to_user=following).save()
			return JsonResponse({'status':'ok'})

# ============================================= unfollow ======================================================
@login_required
def unfollow(request,user_id):
	if request.method == 'POST':
		user_id = request.POST['user_id']
		following = get_object_or_404(UserModel, pk=user_id)
		check_relation = Relation.objects.filter(from_user=request.user, to_user=following)
		if check_relation.exists():
			check_relation.delete()
			return JsonResponse({'status':'ok'})
		else:
			return JsonResponse({'status':'notexists'})



@login_required
def bookmark(request, id, slug):
    pk = request.POST.get('post_id')
    post = get_object_or_404(Post, id = pk)
    profile = get_object_or_404(Profile, id = request.user.id)
    if profile.bookmark.filter(id =post.id).exists():
        profile.bookmark.remove(post)
    else:
        profile.bookmark.add(post)
    return redirect('blog:detail', id=id, slug=slug)




@login_required
def user_bookmark(request):
    bookmark = request.user.profile.bookmark.all()
    return render(request, 'user/bookmark.html',{'bookmark':bookmark})
    


def follower(request, id):
    user = get_object_or_404(Profile, user_id=id)
    followers = user.followers.all()
    return render(request, 'user/follower.html', {'followers':followers,})

def following(request, id):
    user = get_object_or_404(Profile, user_id=id)
    following = user.following.all()
    return render(request, 'user/following.html', {'following':following,})

