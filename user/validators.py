from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def GithubValidator(value):
    if "github.com" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your github link should be like example.github.com'),
            params={'value': value},
        )


def GitlabValidator(value):
    if "gitlab.com" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your gitlab link should be like example.gitlab.com'),
            params={'value': value},
        )

def LinkedinValidator(value):
    if "linkedin.com/in/" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your linkedin ID should be like linkedin.com/in/example'),
            params={'value': value},
        )

def TelegramValidator(value):
    if "t.me" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your Telegram link should be like t.me/yourID'),
            params={'value': value},
        )


def InstagramValidator(value):
    if "instagram.com/" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your Instagram link should be like instagram.com/yourID'),
            params={'value': value},
        )


def TwitterValidator(value):
    if "twitter.com/" not in value:
        raise ValidationError(
            _('%(value)s is not valid. your Twiiter link should be like twitter.com/yourID'),
            params={'value': value},
        )
