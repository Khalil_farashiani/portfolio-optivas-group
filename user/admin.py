from django.contrib import admin
from .models import UserImage, social, Profile, Relation

# Register your models here.

admin.site.register(UserImage)
admin.site.register(social)
admin.site.register(Profile)
admin.site.register(Relation)
